const BASE52 = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

const fs = require('fs');
const BSON = require('bson');
const bs52 = require('base-x')(BASE52);
const { isPlainObject } = require('is-plain-object');
const { MongoClient } = require('mongodb');

const URI = 'mongodb://localhost?retryWrites=true&w=majority';

const client = new MongoClient(URI);

const pickStats = ({ns, size, count, avgObjSize, storageSize, totalIndexSize, totalSize, indexSizes}) => ({ns, size, count, avgObjSize, storageSize, totalIndexSize, totalSize, indexSizes});

let i = 0;
const fieldMap = new Map();
function assureShortField(fieldPath) {
  if (fieldMap.has(fieldPath)) {
    return;
  }

  const numbers = [];
  let j = i;
  while (j !== 0 || !numbers.length) {
    numbers.push(j % 256);
    j >>= 8;
  }
  fieldMap.set(fieldPath, bs52.encode(numbers));
  i++;
}

function convertToShortFields(doc, path) {
  if (!isPlainObject(doc)) {
    return doc;
  }

  const result = {};
  for (let [field, value] of Object.entries(doc)) {
    const fieldPath = path + '.' + field;
    if (field === '_id') {
      result[field] = convertToShortFields(value, fieldPath);
      continue;
    }

    assureShortField(fieldPath);

    result[fieldMap.get(fieldPath)] = convertToShortFields(value, fieldPath);
  }

  return result;
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function run() {
  try {
    await client.connect();
    const database = client.db('benchmark');
    const longFields = database.collection('longFields');
    const shortFields = database.collection('shortFields');

    console.log("Preparing data to insert.");
    const longFieldsDocuments = [];
    const messagesBson = fs.readFileSync('enron/messages.bson');
    BSON.deserializeStream(messagesBson, 0, 120477, longFieldsDocuments, 0);
    const shortFieldsDocuments = [];
    for (let i = 0; i < longFieldsDocuments.length; i++) {
      shortFieldsDocuments.push(convertToShortFields(longFieldsDocuments[i], ''));
    }

    console.log("Inserting.");
    await longFields.insertMany(longFieldsDocuments);
    await shortFields.insertMany(shortFieldsDocuments);

    // We have to wait for a bit before stats return real numbers.
    // See: https://jira.mongodb.org/browse/WT-6887
    console.log("Waiting for a bit.");
    await sleep(60 * 1000);

    console.log("Done.");
    const longFieldsStats = await longFields.stats();
    const shortFieldsStats = await shortFields.stats();

    console.dir(pickStats(longFieldsStats));
    console.dir(pickStats(shortFieldsStats));
  } finally {
    await client.close();
  }
}

run().catch(console.dir);

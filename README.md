Start MongoDB:

```sh
$ docker run --rm -d --name mongo -p 27017:27017 mongo:4.4.1 --wiredTigerCollectionBlockCompressor <compression>
```

For `<compression>` use `none`, `zlib`, `snappy`, or `zstd`. See
[documentation here](https://docs.mongodb.com/manual/reference/configuration-options/#storage.wiredTiger.collectionConfig.blockCompressor).

Results of storing [Enron dataset](./enron), once with original (long) field names and once
with them converted to as short as possible field names:

| | Long none | Short none | Long snappy | Short snappy | Long zlib | Short zlib | Long zstd | Short zstd |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
| **size** | 396236668 | 379767314 | 396236668 | 379767314 | 396236668 | 379767314 | 396236668 | 379767314 |
| **count** | 120477 | 120477 | 120477 | 120477 | 120477 | 120477 | 120477 | 120477 |
| **avgObjSize** | 3288 | 3152 | 3288 | 3152 | 3288 | 3152 | 3288 | 3152 |
| **storageSize** | 417001472 | 399450112 | 202596352 | 201023488 | 130277376 | 130658304 | 122458112 | 122908672 |
| **totalIndexSize** | 1101824 | 1101824 | 1101824 | 1101824 | 1101824 | 1101824 | 1101824 | 1097728 |
| **totalSize** | 418103296 | 400551936 | 203698176 | 202125312 | 131379200 | 131760128 | 123559936 | 124006400 |

Compression works very well on this dataset. zstd compresses the most. It is interesting
to note that for zlib and zstd compression algorithms original (long) field names
compressed better overall than short field names.

See also [this blog post about compression in MongoDB](https://www.mongodb.com/blog/post/new-compression-options-mongodb-30).
